const validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateWorkerInput(data) {
    let errors = {};

    if(isEmpty(data)) data = {};

    const genders = ['male', 'female', 'other'];

    data.first_name = !isEmpty(data.first_name) ? data.first_name : '';
    data.last_name = !isEmpty(data.last_name) ? data.last_name : '';
    data.gender = !isEmpty(data.gender) ? data.gender : '';
    data.phone = !isEmpty(data.phone) ? data.phone : '';
    data.salary = !isEmpty(data.salary) ? data.salary : '';
    data.position = !isEmpty(data.position) ? data.position : '';

    if (!validator.isLength(data.first_name, {min: 2, max: 30})) {
        errors.first_name = 'First name must be between 2 and 30 characters';
    }

    if (validator.isEmpty(data.first_name)) {
        errors.first_name = 'First name field is required';
    }

    if (!validator.isLength(data.last_name, {min: 2, max: 30})) {
        errors.last_name = 'Last name must be between 2 and 30 characters';
    }

    if (validator.isEmpty(data.last_name)) {
        errors.last_name = 'Last name field is required';
    }

    if (!validator.isEmpty(data.gender) && genders.indexOf(data.gender) === -1) {
        errors.text = 'Gender must be `male`, `female` or `other`';
    }

    if (validator.isEmpty(data.gender)) {
        errors.gender = 'Gender field is required';
    }

    if (validator.isEmpty(data.phone)) {
        errors.phone = 'Phone field is required';
    }

    if (!validator.isEmpty(data.salary) && (!validator.isFloat(data.salary) || !validator.isInt(data.salary))) {
        errors.salary = 'Salary field is not valid';
    }

    return {
        errors,
        isValid: isEmpty(errors)
    }
};
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const WorkerSchema = new Schema({
    first_name: {
        type: String,
        required: true
    },
    last_name: {
        type: String,
        required: true
    },
    gender: {
        type: String,
        enum: ['male', 'female', 'other']
    },
    phone: {
        type: String,
        required: true,
        max: 20
    },
    salary: {
        type: String
    },
    position: {
        type: String
    },
    user: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'users'
    },
    createdDate: {
        type: Date,
        default: Date.now
    }
});

module.exports = Worker = mongoose.model('workers', WorkerSchema);
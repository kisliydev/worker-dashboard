import { createMuiTheme } from '@material-ui/core/styles';

export default createMuiTheme({
    palette: {
        primary: {
            main: '#2e7d32',
        },
        secondary: {
            main: '#0c5d55',
        },
    },
    typography: {
        useNextVariants: true
    }
});
import axios from 'axios';

import {
    SEARCH_WORKER,
    ADD_WORKER,
    GET_ERRORS,
    GET_WORKERS,
    GET_WORKER,
    WORKER_LOADING,
    DELETE_WORKER,
    DELETE_WORKERS,
    EDIT_WORKER
} from './types';

import { API_VERSION }                from "../constants";
import { successNotify, errorNotify } from "./notifyActions";
import { clearErrors }                from "./errorActions";

// Add Worker
export const addWorker = data => dispatch => {
    dispatch(clearErrors());
    axios
        .post(`/api/${API_VERSION}/workers`, data)
        .then(res => {
                dispatch({
                    type   : ADD_WORKER,
                    payload: res.data
                });
            }
        )
        .catch(err =>
            dispatch({
                type   : GET_ERRORS,
                payload: err.response.data
            })
        );
};

// Update Worker
export const editWorker = data => dispatch => {
    dispatch(clearErrors());
    axios
        .put(`/api/${API_VERSION}/workers/${data._id}`, data)
        .then(() => {
                dispatch({
                    type   : EDIT_WORKER,
                    payload: data
                });
            }
        )
        .catch(err =>
            dispatch({
                type   : GET_ERRORS,
                payload: err.response.data
            })
        );
};

// Get Workers
export const getWorkers = () => dispatch => {
    dispatch(setWorkerLoading());
    axios
        .get(`/api/${API_VERSION}/workers`)
        .then(res =>
            dispatch({
                type   : GET_WORKERS,
                payload: res.data
            })
        )
        .catch(() =>
            dispatch({
                type   : GET_WORKERS,
                payload: {}
            })
        );
};

// Get Worker
export const getWorker = id => dispatch => {
    dispatch(setWorkerLoading());
    axios
        .get(`/api/${API_VERSION}/workers/${id}`)
        .then(res =>
            dispatch(setWorker(res.data))
        )
        .catch(() =>
            dispatch(setWorker(null))
        );
};

// Delete Worker
export const deleteWorker = id => dispatch => {
    axios
        .delete(`/api/${API_VERSION}/workers/${id}`)
        .then(() => {
                dispatch({
                    type   : DELETE_WORKER,
                    payload: id
                });
                dispatch(successNotify('Worker successfully deleted'));
            }
        )
        .catch(err => {
                dispatch({
                    type   : GET_ERRORS,
                    payload: err.response.data
                });
                dispatch(errorNotify('Something went wrong'));
            }
        );
};

// Delete Workers
export const deleteWorkers = ids => dispatch => {
    axios
        .post(`/api/${API_VERSION}/workers/delete`, ids)
        .then(() => {
                dispatch({
                    type   : DELETE_WORKERS,
                    payload: ids
                });
                dispatch(successNotify('Workers successfully deleted'));
            }
        )
        .catch(err => {
                dispatch({
                    type   : GET_ERRORS,
                    payload: err.response.data
                });
                dispatch(errorNotify('Something went wrong'));
            }
        );
};

// Set worker to state
export const searchWorkers = data => {
    return {
        type   : SEARCH_WORKER,
        payload: data
    };
};

// Set worker to state
export const setWorker = data => {
    return {
        type   : GET_WORKER,
        payload: data
    };
};

// Set loading state
export const setWorkerLoading = () => {
    return {
        type: WORKER_LOADING
    };
};

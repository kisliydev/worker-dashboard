export const GET_ERRORS = 'GET_ERRORS';
export const CLEAR_ERRORS = 'CLEAR_ERRORS';
export const SET_CURRENT_USER = 'SET_CURRENT_USER';

export const WORKER_LOADING = 'WORKER_LOADING';
export const SEARCH_WORKER = 'SEARCH_WORKER';
export const GET_WORKERS = 'GET_WORKERS';
export const GET_WORKER = 'GET_WORKER';
export const ADD_WORKER = 'ADD_WORKER';
export const EDIT_WORKER = 'EDIT_WORKER';
export const DELETE_WORKER = 'DELETE_WORKER';
export const DELETE_WORKERS = 'DELETE_WORKERS';

export const SHOW_SUCCESS_NOTIFY = 'SHOW_SUCCESS_NOTIFY';
export const SHOW_ERROR_NOTIFY = 'SHOW_ERROR_NOTIFY';
export const CLOSE_NOTIFY = 'CLOSE_NOTIFY';

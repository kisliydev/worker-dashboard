import { CLOSE_NOTIFY, SHOW_ERROR_NOTIFY, SHOW_SUCCESS_NOTIFY } from './types';

export const successNotify = (message) => {
  return {
      type   : SHOW_SUCCESS_NOTIFY,
      payload: message
  }
};

export const errorNotify = (message) => {
  return {
      type   : SHOW_ERROR_NOTIFY,
      payload: message
  }
};

export const closeNotify = () => dispatch => {
    dispatch({
        type: CLOSE_NOTIFY
    });
};
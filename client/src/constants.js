export const API_VERSION = 'v1';
export const NOTIFY_HIDE_DURATION = 3000; // ms
export const WAIT_INTERVAL = 500; // ms - For search fields

import { SHOW_SUCCESS_NOTIFY, SHOW_ERROR_NOTIFY, CLOSE_NOTIFY } from '../actions/types';

const initialState = {
    open   : false,
    type   : 'success',
    message: ''
};

export default function (state = initialState, action) {
    switch (action.type) {
        case SHOW_SUCCESS_NOTIFY:
            return {
                ...state,
                open: true,
                type: 'success',
                message: action.payload
            };
        case SHOW_ERROR_NOTIFY:
            return {
                ...state,
                open: true,
                type: 'error',
                message: action.payload
            };
        case CLOSE_NOTIFY:
            return {
                ...state,
                open: false
            };
        default:
            return state;
    }
}

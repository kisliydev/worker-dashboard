import { combineReducers } from 'redux';
import authReducer         from './authReducer';
import errorReducer        from './errorReducer';
import workerReducer         from './workerReducer';
import notifyReducer         from './notifyReducer';

export default combineReducers({
  auth: authReducer,
  errors: errorReducer,
  worker: workerReducer,
  notify: notifyReducer,
});

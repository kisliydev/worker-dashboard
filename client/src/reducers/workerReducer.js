import {
    SEARCH_WORKER,
    ADD_WORKER,
    GET_WORKERS,
    GET_WORKER,
    DELETE_WORKER,
    DELETE_WORKERS,
    EDIT_WORKER,
    WORKER_LOADING
}                        from '../actions/types';
import { filterByValue } from "../utils/sort";

const initialState = {
    workers        : [],
    originalWorkers: [], // When workers is filtered this value used for filtering always full data
    worker         : {},
    loading        : false
};

export default function (state = initialState, action) {
    switch (action.type) {
        case WORKER_LOADING:
            return {
                ...state,
                loading: true
            };
        case SEARCH_WORKER:
            return {
                ...state,
                workers: filterByValue(state.originalWorkers, action.payload)
            };
        case GET_WORKERS:
            return {
                ...state,
                workers        : action.payload,
                originalWorkers: action.payload,
                loading        : false
            };
        case GET_WORKER:
            return {
                ...state,
                worker : action.payload,
                loading: false
            };
        case ADD_WORKER:
            return {
                ...state,
                worker : {},
                workers: [ action.payload, ...state.workers ],
                originalWorkers: [ action.payload, ...state.originalWorkers ]
            };
        case EDIT_WORKER:
            const index = state.workers.findIndex((obj => obj._id === action.payload._id));
            if (index === -1) {
                return state;
            }
            let workers = [ ...state.workers ];
            workers[ index ] = action.payload;

            return {
                ...state,
                worker : {},
                workers: workers
            };
        case DELETE_WORKER:
            return {
                ...state,
                workers: state.workers.filter(worker => worker._id !== action.payload),
                originalWorkers: state.originalWorkers.filter(worker => worker._id !== action.payload)
            };
        case DELETE_WORKERS:
            return {
                ...state,
                workers: state.workers.filter(worker => !action.payload.includes(worker._id)),
                originalWorkers: state.originalWorkers.filter(worker => !action.payload.includes(worker._id))
            };
        default:
            return state;
    }
}

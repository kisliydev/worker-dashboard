import React, { Component }                    from 'react';
import PropTypes                               from 'prop-types';
import { connect }                             from 'react-redux';
import { withStyles }                          from '@material-ui/core/styles';
import Table                                   from '@material-ui/core/Table';
import TableFooter                             from '@material-ui/core/TableFooter';
import TablePagination                         from '@material-ui/core/TablePagination';
import Typography                              from '@material-ui/core/Typography';
import TableRow                                from '@material-ui/core/TableRow';
import Paper                                   from '@material-ui/core/Paper';
import WorkerTableToolBar                      from "./WorkerTableToolBar";
import WorkerTableHeader                       from "./WorkerTableHeader";
import WorkerTableBody                         from "./WorkerTableBody";
import WorkerTablePaginationAction             from "./WorkerTablePaginationAction";
import WorkerDialog                            from "./WorkerDialog";
import Spinner                                 from '../common/Spinner';
import ConfirmDialog, { defaultConfirmDialog } from "../common/confirmDialog";

import { getSorting, stableSort } from '../../utils/sort';
import {
    getWorkers,
    setWorker,
    editWorker,
    deleteWorker,
    deleteWorkers,
    addWorker,
    searchWorkers
}                                 from '../../actions/workerActions';
import { clearErrors }            from '../../actions/errorActions';
import isEmpty                    from "../../validation/is-empty";
import { WAIT_INTERVAL }          from "../../constants";

class Workers extends Component {
    constructor(props) {
        super(props);

        this.state = {
            workers      : [],
            order        : 'asc',
            orderBy      : 'first_name',
            search       : '',
            selected     : [],
            page         : 0,
            rowsPerPage  : 5,
            workerDialog : false,
            confirmDialog: defaultConfirmDialog
        };
    }

    componentDidMount() {
        this.props.getWorkers();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.worker !== this.props.worker) {
            this.setState({
                workers     : this.props.worker.workers,
                selected    : [],
                workerDialog: !isEmpty(this.props.worker.worker)
            });
        }
    }

    handleRequestSort = (e, property) => {
        this.setState({
            order  : (this.state.orderBy === property && this.state.order === 'desc') ? 'asc' : 'desc',
            orderBy: property
        });
    };

    handleSelectAllClick = e => {
        this.setState({
            selected: !!e.target.checked ? this.state.workers.map(row => row._id) : []
        });
    };

    handleChangePage = (e, page) => {
        this.setState({
            page
        });
    };

    handleChangeRowsPerPage = e => {
        this.setState({
            page       : 0,
            rowsPerPage: parseInt(e.target.value, 10)
        });
    };

    handleRowClick = (e, id) => {
        const { selected } = this.state;
        const selectedIndex = selected.indexOf(id);
        let newSelected = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, id);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }

        this.setState({
            selected: newSelected
        });
    };

    handleEditClick = (e, worker) => {
        e.preventDefault();
        e.stopPropagation();

        this.props.setWorker(worker);
    };

    handleDeleteClick = (e, id) => {
        e.preventDefault();
        e.stopPropagation();

        this.confirmDeleteWorker(id);
    };

    handleDeleteSelectedClick = () => {
        this.confirmDeleteWorker(this.state.selected);
    };

    confirmDeleteWorker = (id) => {
        if (isEmpty(id)) {
            return false;
        }

        this.setState({
            confirmDialog: {
                open      : true,
                title     : 'Confirm',
                message   : 'Are you sure you want to delete this worker(s)?',
                onDisagree: () => {
                    this.setState({
                        confirmDialog: defaultConfirmDialog
                    })
                },
                onAgree   : () => {
                    if (Array.isArray(id)) {
                        this.props.deleteWorkers(id);
                    } else {
                        this.props.deleteWorker(id);
                    }
                    this.setState({
                        confirmDialog: defaultConfirmDialog
                    })
                }

            }
        })
    };

    handleSearch = (e) => {
        const value = e.target.value;
        this.setState({
            search: value
        });

        clearTimeout(this.timer);

        this.timer = setTimeout(() => {
            this.props.searchWorkers(value);
        }, WAIT_INTERVAL);
    };

    handleAddClick = () => {
        this.setState({
            workerDialog: true
        })
    };

    handleCloseWorkerDialog = () => {
        this.props.clearErrors();
        this.props.setWorker({});
    };

    handleSubmitWorkerDialog = (e, worker) => {
        e.preventDefault();
        e.stopPropagation();

        if (isEmpty(worker._id)) {
            // Add new worker
            this.props.addWorker(worker)
        } else {
            // Update worker
            this.props.editWorker(worker)
        }
    };

    render() {
        const { classes } = this.props;
        const { loading, worker } = this.props.worker;
        const { workers, order, search, orderBy, selected, rowsPerPage, page, workerDialog, confirmDialog } = this.state;
        let content = '';

        if (loading)
            return <Spinner/>;

        if (workers && workers.length > 0) {
            content = (
                <Table>
                    <WorkerTableHeader
                        cntSelected={selected.length}
                        order={order}
                        orderBy={orderBy}
                        onSelectAllClick={this.handleSelectAllClick}
                        onRequestSort={this.handleRequestSort}
                        rowCount={workers.length}
                    />
                    <WorkerTableBody
                        workers={
                            stableSort(workers, getSorting(order, orderBy))
                                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        }
                        selected={selected}
                        onRowClick={this.handleRowClick}
                        onEditClick={this.handleEditClick}
                        onDeleteClick={this.handleDeleteClick}
                    />
                    <TableFooter>
                        <TableRow>
                            <TablePagination
                                count={workers.length}
                                rowsPerPage={rowsPerPage}
                                page={page}
                                SelectProps={{
                                    native: true,
                                }}
                                onChangePage={this.handleChangePage}
                                onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                ActionsComponent={WorkerTablePaginationAction}
                            />
                        </TableRow>
                    </TableFooter>
                </Table>
            );
        } else {
            content = (
                <Typography className={classes.not_found} variant="h6" align="center" color="textSecondary">
                    Not found
                </Typography>
            );
        }

        return (
            <Paper className={classes.root}>
                <div className={classes.tableWrapper}>
                    <WorkerTableToolBar
                        search={search}
                        cntSelected={selected.length}
                        onDeleteSelectedClick={this.handleDeleteSelectedClick}
                        onAddClick={this.handleAddClick}
                        onSearch={this.handleSearch}
                    />
                    {content}
                </div>
                <WorkerDialog
                    open={workerDialog}
                    worker={worker}
                    onClose={this.handleCloseWorkerDialog}
                    onSubmit={this.handleSubmitWorkerDialog}
                />
                <ConfirmDialog
                    open={confirmDialog.open}
                    title={confirmDialog.title}
                    message={confirmDialog.message}
                    onDisagree={confirmDialog.onDisagree}
                    onAgree={confirmDialog.onAgree}
                />
            </Paper>
        );
    }
}

const
    styles = theme => ({
        root        : {
            width      : '100%',
            minWidth   : 500,
            maxWidth   : 1200,
            marginLeft : 'auto',
            marginRight: 'auto',
            marginTop  : theme.spacing.unit * 8
        },
        tableWrapper: {
            overflowX: 'auto'
        },
        not_found   : {
            margin: theme.spacing.unit * 2
        }
    });

Workers.propTypes = {
    classes      : PropTypes.object.isRequired,
    getWorkers   : PropTypes.func.isRequired,
    setWorker    : PropTypes.func.isRequired,
    editWorker   : PropTypes.func.isRequired,
    addWorker    : PropTypes.func.isRequired,
    deleteWorker : PropTypes.func.isRequired,
    deleteWorkers: PropTypes.func.isRequired,
    worker       : PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    worker: state.worker
});

const mapDispatchToProps = {
    getWorkers,
    setWorker,
    editWorker,
    addWorker,
    deleteWorker,
    deleteWorkers,
    searchWorkers,
    clearErrors
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Workers));
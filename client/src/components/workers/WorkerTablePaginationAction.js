import React, { Component } from 'react';
import PropTypes            from 'prop-types';
import { withStyles }       from '@material-ui/core/styles';
import IconButton           from '@material-ui/core/IconButton';
import FirstPageIcon        from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft    from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight   from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon         from '@material-ui/icons/LastPage';

class WorkerTablePaginationAction extends Component {
    handleFirstPageButtonClick = e => {
        this.props.onChangePage(e, 0);
    };

    handleBackButtonClick = e => {
        this.props.onChangePage(e, this.props.page - 1);
    };

    handleNextButtonClick = e => {
        this.props.onChangePage(e, this.props.page + 1);
    };

    handleLastPageButtonClick = e => {
        this.props.onChangePage(
            e,
            Math.max(0, Math.ceil(this.props.count / this.props.rowsPerPage) - 1)
        );
    };

    render() {
        const { classes, count, page, rowsPerPage } = this.props;

        return (
            <div className={classes.root}>
                <IconButton
                    onClick={this.handleFirstPageButtonClick}
                    disabled={page === 0}
                    aria-label="First Page"
                >
                    <FirstPageIcon/>
                </IconButton>
                <IconButton
                    onClick={this.handleBackButtonClick}
                    disabled={page === 0}
                    aria-label="Previous Page"
                >
                    <KeyboardArrowLeft/>
                </IconButton>
                <IconButton
                    onClick={this.handleNextButtonClick}
                    disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                    aria-label="Next Page"
                >
                    <KeyboardArrowRight/>
                </IconButton>
                <IconButton
                    onClick={this.handleLastPageButtonClick}
                    disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                    aria-label="Last Page"
                >
                    <LastPageIcon/>
                </IconButton>
            </div>
        );
    }
}

const styles = theme => ({
    root: {
        flexShrink: 0,
        color     : theme.palette.text.secondary,
        marginLeft: theme.spacing.unit * 2.5
    }
});

WorkerTablePaginationAction.propTypes = {
    classes     : PropTypes.object.isRequired,
    count       : PropTypes.number.isRequired,
    onChangePage: PropTypes.func.isRequired,
    page        : PropTypes.number.isRequired,
    rowsPerPage : PropTypes.number.isRequired
};

export default withStyles(styles)(WorkerTablePaginationAction);
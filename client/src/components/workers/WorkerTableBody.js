import React      from 'react';
import PropTypes  from 'prop-types';
import TableBody  from '@material-ui/core/TableBody';
import TableCell  from '@material-ui/core/TableCell';
import Checkbox   from '@material-ui/core/Checkbox';
import TableRow   from '@material-ui/core/TableRow';
import IconButton from '@material-ui/core/IconButton';
import CreateIcon from '@material-ui/icons/Create';
import DeleteIcon from '@material-ui/icons/Delete';

const WorkerTableBody = props => {
    const { workers, onRowClick, onEditClick, onDeleteClick } = props;

    return (
        <TableBody>
            {workers.map(row => {
                const isSelected = props.selected.indexOf(row._id) !== -1;

                return (
                    <TableRow
                        hover
                        onClick={e => onRowClick(e, row._id)}
                        role="checkbox"
                        aria-checked={isSelected}
                        tabIndex={-1}
                        key={row._id}
                        selected={isSelected}
                    >
                        <TableCell padding="checkbox">
                            <Checkbox checked={isSelected}/>
                        </TableCell>
                        <TableCell padding="none">{row.first_name}</TableCell>
                        <TableCell padding="none">{row.last_name}</TableCell>
                        <TableCell padding="none">{row.gender}</TableCell>
                        <TableCell padding="none">{row.phone}</TableCell>
                        <TableCell padding="none">{row.position}</TableCell>
                        <TableCell padding="none">{row.salary}</TableCell>
                        <TableCell padding="none">
                            {new Date(row.createdDate).toLocaleDateString()}
                        </TableCell>
                        <TableCell padding="none">
                            <IconButton aria-label="Edit" onClick={e => onEditClick(e, row)}>
                                <CreateIcon className="EditCell"/>
                            </IconButton>
                        </TableCell>
                        <TableCell padding="none">
                            <IconButton aria-label="Delete" onClick={e => onDeleteClick(e, row._id)} className="DeleteCell">
                                <DeleteIcon/>
                            </IconButton>
                        </TableCell>
                    </TableRow>
                );
            })}
        </TableBody>
    );
};

WorkerTableBody.propTypes = {
    workers      : PropTypes.array.isRequired,
    selected     : PropTypes.array.isRequired,
    onRowClick   : PropTypes.func.isRequired,
    onEditClick  : PropTypes.func.isRequired,
    onDeleteClick: PropTypes.func.isRequired
};

export default WorkerTableBody;
import React, { Fragment } from 'react';
import PropTypes           from 'prop-types';
import { withStyles }      from '@material-ui/core/styles';
import Grid                from '@material-ui/core/Grid';
import Input               from '@material-ui/core/Input';
import InputAdornment      from '@material-ui/core/InputAdornment';
import IconButton          from '@material-ui/core/IconButton';
import Tooltip             from '@material-ui/core/Tooltip';
import Toolbar             from '@material-ui/core/Toolbar';
import Typography          from '@material-ui/core/Typography';
import DeleteIcon          from '@material-ui/icons/Delete';
import AddIcon             from '@material-ui/icons/PersonAdd';
import SearchIcon          from '@material-ui/icons/Search';

const WorkerTableToolBar = props => {
    const { onDeleteSelectedClick, onAddClick, onSearch, cntSelected, search, classes } = props;

    return (
        <Toolbar className={cntSelected > 0 ? classes.highlight : ''}>
            <div className={classes.title}>
                {cntSelected > 0 ? (
                    <Typography color="inherit" variant="subtitle1">
                        {cntSelected} selected
                    </Typography>
                ) : (
                    <Typography variant="h6" id="tableTitle" color="primary">
                        Workers
                    </Typography>
                )}
            </div>
            <div className={classes.spacer}/>
            <div className={classes.actions}>
                {cntSelected > 0 ?
                    <Tooltip title="Delete">
                        <IconButton aria-label="Delete" className={classes.icons} onClick={onDeleteSelectedClick}>
                            <DeleteIcon/>
                        </IconButton>
                    </Tooltip> :
                    <Fragment>
                        <Grid item>
                            <Input
                                value={search ? search : ''}
                                className={classes.search}
                                startAdornment={
                                    <InputAdornment position="start">
                                        <SearchIcon/>
                                    </InputAdornment>
                                }
                                onChange={onSearch}
                            />
                        </Grid>
                        <Grid item>
                            <Tooltip title="Add worker">
                                <IconButton aria-label="Add" color="primary" onClick={onAddClick}>
                                    <AddIcon/>
                                </IconButton>
                            </Tooltip>
                        </Grid>
                    </Fragment>
                }
            </div>
        </Toolbar>
    );
};

const styles = theme => ({
    highlight:
        {
            color          : theme.palette.primary.contrastText,
            backgroundColor: theme.palette.primary.dark
        },
    spacer   : {
        flex: '1 1 100%'
    },
    search   : {
        marginTop: 8
    },
    actions  : {
        color  : theme.palette.primary.contrastText,
        display: 'flex'
    },
    icons    : {
        color: theme.palette.primary.contrastText
    },
    title    : {
        flex: '0 0 auto'
    },
});

WorkerTableToolBar.propTypes = {
    classes              : PropTypes.object.isRequired,
    search               : PropTypes.string,
    cntSelected          : PropTypes.number.isRequired,
    onDeleteSelectedClick: PropTypes.func.isRequired,
    onAddClick           : PropTypes.func.isRequired,
    onSearch             : PropTypes.func.isRequired
};

export default withStyles(styles)(WorkerTableToolBar);
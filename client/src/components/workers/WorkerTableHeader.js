import React, { Component } from 'react';
import PropTypes            from 'prop-types';
import TableHead            from '@material-ui/core/TableHead';
import TableCell            from '@material-ui/core/TableCell';
import Checkbox             from '@material-ui/core/Checkbox';
import TableRow             from '@material-ui/core/TableRow';
import Tooltip              from '@material-ui/core/Tooltip';
import TableSortLabel       from '@material-ui/core/TableSortLabel';

const rows = [
    { id: 'first_name', label: 'First Name' },
    { id: 'last_name', label: 'Last Name' },
    { id: 'gender', label: 'Gender' },
    { id: 'phone', label: 'Phone' },
    { id: 'position', label: 'Position' },
    { id: 'salary', label: 'Salary ($)' },
    { id: 'createdDate', label: 'Created Date' }
];

class WorkerTableHeader extends Component {
    createSortHandler = property => e => {
        this.props.onRequestSort(e, property);
    };

    render() {
        const { onSelectAllClick, order, orderBy, cntSelected, rowCount } = this.props;

        return (
            <TableHead>
                <TableRow>
                    <TableCell padding="checkbox">
                        <Checkbox
                            indeterminate={cntSelected > 0 && cntSelected < rowCount}
                            checked={cntSelected === rowCount}
                            onChange={onSelectAllClick}
                        />
                    </TableCell>
                    {rows.map(
                        row => (
                            <TableCell
                                key={row.id}
                                sortDirection={orderBy === row.id ? order : false}
                                padding="none"
                            >
                                <Tooltip
                                    title="Sort"
                                    enterDelay={300}
                                >
                                    <TableSortLabel
                                        active={orderBy === row.id}
                                        direction={order}
                                        onClick={this.createSortHandler(row.id)}
                                    >
                                        {row.label}
                                    </TableSortLabel>
                                </Tooltip>
                            </TableCell>
                        ),
                        this
                    )}
                    <TableCell/>
                    <TableCell/>
                </TableRow>
            </TableHead>
        );
    }
}

WorkerTableHeader.propTypes = {
    cntSelected     : PropTypes.number.isRequired,
    onRequestSort   : PropTypes.func.isRequired,
    onSelectAllClick: PropTypes.func.isRequired,
    order           : PropTypes.string.isRequired,
    orderBy         : PropTypes.string.isRequired,
    rowCount        : PropTypes.number.isRequired
};

export default WorkerTableHeader;
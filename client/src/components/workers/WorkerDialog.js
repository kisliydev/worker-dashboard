import React, { Component } from 'react';
import { connect }          from "react-redux";
import PropTypes            from 'prop-types';
import Button               from '@material-ui/core/Button';
import Dialog               from '@material-ui/core/Dialog';
import DialogActions        from '@material-ui/core/DialogActions';
import DialogContent        from '@material-ui/core/DialogContent';
import DialogTitle          from '@material-ui/core/DialogTitle';
import FormHelperText       from "@material-ui/core/FormHelperText";
import FormControlLabel     from "@material-ui/core/FormControlLabel";
import FormControl          from '@material-ui/core/FormControl';
import FormLabel            from '@material-ui/core/FormLabel';
import Input                from '@material-ui/core/Input';
import InputLabel           from '@material-ui/core/InputLabel';
import Radio                from '@material-ui/core/Radio';
import RadioGroup           from '@material-ui/core/RadioGroup';
import withStyles           from "@material-ui/core/styles/withStyles";

import isEmpty           from "../../validation/is-empty";
import MaskedInputMobile from "../common/MaskedInputMobile";

const defaultWorker = {
    worker     : {},
    _id        : '',
    first_name : '',
    last_name  : '',
    gender     : '',
    phone      : '',
    salary     : '',
    position   : '',
    createdDate: ''
};

class WorkerDialog extends Component {
    constructor(props) {
        super(props);

        this.state = {
            errors: {},
            ...defaultWorker
        };
    }

    componentDidUpdate(prevProps) {
        if (prevProps.worker !== this.props.worker) {
            this.setWorkerData(this.props.worker);
        }

        if (prevProps.errors !== this.props.errors) {
            this.setState({
                errors: this.props.errors
            });
        }
    }

    setWorkerData = (worker) => {
        this.setState(
            isEmpty(worker) ?
                defaultWorker :
                {
                    worker     : worker,
                    _id        : worker._id,
                    first_name : worker.first_name,
                    last_name  : worker.last_name,
                    gender     : worker.gender,
                    phone      : worker.phone,
                    salary     : worker.salary,
                    position   : worker.position,
                    createdDate: worker.createdDate
                }
        );
    };

    getWorkerData = () => {
        return {
            _id        : this.state._id,
            first_name : this.state.first_name,
            last_name  : this.state.last_name,
            gender     : this.state.gender,
            phone      : this.state.phone,
            salary     : this.state.salary,
            position   : this.state.position,
            createdDate: this.state.createdDate
        }
    };

    onChange = (e) => {
        this.setState({
            errors           : {},
            [ e.target.name ]: e.target.value
        });
    };

    render() {
        const { classes, open, onClose, onSubmit } = this.props;
        const { worker, errors, first_name, last_name, gender, phone, salary, position } = this.state;
        const isNew = isEmpty(worker);

        return (
            <Dialog
                open={open}
                onClose={onClose}
                maxWidth='xs'
                aria-labelledby="form-dialog-title"
            >
                <form noValidate onSubmit={(e) => onSubmit(e, this.getWorkerData())}>
                    <DialogTitle
                        id="form-dialog-title">{isNew ? 'Add worker' : 'Edit worker'}</DialogTitle>
                    <DialogContent>

                        <FormControl margin="normal" required fullWidth error={!!errors.first_name}>
                            <InputLabel htmlFor="first_name">First name</InputLabel>
                            <Input
                                id="first_name"
                                name="first_name"
                                autoComplete="first_name"
                                autoFocus
                                value={first_name}
                                onChange={this.onChange}
                            />
                            {errors.first_name && <FormHelperText>{errors.first_name}</FormHelperText>}
                        </FormControl>
                        <FormControl margin="normal" required fullWidth error={!!errors.last_name}>
                            <InputLabel htmlFor="last_name">Last name</InputLabel>
                            <Input
                                id="last_name"
                                name="last_name"
                                autoComplete="last_name"
                                value={last_name}
                                onChange={this.onChange}
                            />
                            {errors.last_name && <FormHelperText>{errors.last_name}</FormHelperText>}
                        </FormControl>
                        <FormControl margin="normal" required fullWidth className={classes.mb0} error={!!errors.gender}>
                            <FormLabel component="legend">Gender</FormLabel>
                            <RadioGroup
                                aria-label="Gender"
                                name="gender"
                                value={gender}
                                onChange={this.onChange}
                                row
                            >
                                <FormControlLabel value="female" control={<Radio/>} label="Female"/>
                                <FormControlLabel value="male" control={<Radio/>} label="Male"/>
                                <FormControlLabel value="other" control={<Radio/>} label="Other"/>
                            </RadioGroup>
                            {errors.gender && <FormHelperText>{errors.gender}</FormHelperText>}
                        </FormControl>
                        <FormControl margin="normal" required fullWidth className={classes.mt0} error={!!errors.phone}>
                            <InputLabel htmlFor="phone" shrink={true}>Phone</InputLabel>
                            <Input
                                id="phone"
                                name="phone"
                                value={phone}
                                inputComponent={MaskedInputMobile}
                                onChange={this.onChange}
                            />
                            {errors.phone && <FormHelperText>{errors.phone}</FormHelperText>}
                        </FormControl>
                        <FormControl margin="normal" fullWidth error={!!errors.salary}>
                            <InputLabel htmlFor="salary">Salary ($)</InputLabel>
                            <Input
                                id="salary"
                                name="salary"
                                autoComplete="salary"
                                value={salary}
                                onChange={this.onChange}
                            />
                            {errors.salary && <FormHelperText>{errors.salary}</FormHelperText>}
                        </FormControl>
                        <FormControl margin="normal" fullWidth error={!!errors.position}>
                            <InputLabel htmlFor="position">Position</InputLabel>
                            <Input
                                id="position"
                                name="position"
                                autoComplete="position"
                                value={position}
                                onChange={this.onChange}
                            />
                            {errors.position && <FormHelperText>{errors.position}</FormHelperText>}
                        </FormControl>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={onClose} color="primary" variant="outlined">
                            Cancel
                        </Button>
                        <Button type="submit" color="primary" variant="contained">
                            {isNew ? 'Add' : 'Update  '}
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        );
    }
}

const styles = () => ({
    mt0: {
        marginTop: 0
    },
    mb0: {
        marginBottom: 0
    }

});

WorkerDialog.propTypes = {
    classes : PropTypes.object.isRequired,
    worker  : PropTypes.object.isRequired,
    errors  : PropTypes.object.isRequired,
    open    : PropTypes.bool,
    onClose : PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
    errors: state.errors
});

export default connect(mapStateToProps)(withStyles(styles)(WorkerDialog));
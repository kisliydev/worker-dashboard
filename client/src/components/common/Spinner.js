import React            from 'react';
import PropTypes        from 'prop-types';
import { withStyles }   from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

const Spinner = props => {
    const { classes } = props;
    return (
        <div className={classes.wrapper}>
            <CircularProgress className={classes.progress} size={80}/>
        </div>
    );
};

const styles = theme => ({
    wrapper : {
        height        : '100%',
        display       : 'flex',
        justifyContent: 'center',
        alignItems    : 'center'
    },
    progress: {
        marginTop: theme.spacing.unit * 8
    }
});

Spinner.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Spinner);
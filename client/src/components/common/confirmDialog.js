import React             from 'react';
import PropTypes         from 'prop-types';
import Button            from '@material-ui/core/Button';
import Dialog            from '@material-ui/core/Dialog';
import DialogActions     from '@material-ui/core/DialogActions';
import DialogContent     from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle       from '@material-ui/core/DialogTitle';

export const defaultConfirmDialog = {
    open      : false,
    title     : '',
    message   : '',
    onDisagree: () => {},
    onAgree   : () => {}
};

const ConfirmDialog = props => {
    const { open, title, message, onDisagree, onAgree } = props;

    return (
        <Dialog
            open={open}
            onClose={onDisagree}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description">{message}</DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={onDisagree} color="primary" variant="outlined">
                    Disagree
                </Button>
                <Button onClick={onAgree} color="primary" variant="contained">
                    Agree
                </Button>
            </DialogActions>
        </Dialog>
    );
};

ConfirmDialog.propTypes = {
    open      : PropTypes.bool,
    title     : PropTypes.string.isRequired,
    message   : PropTypes.string.isRequired,
    onDisagree: PropTypes.func.isRequired,
    onAgree   : PropTypes.func.isRequired
};

export default ConfirmDialog;
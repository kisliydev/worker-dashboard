import React, { Component }     from 'react';
import PropTypes                from 'prop-types';
import classNames               from 'classnames';
import CheckCircleIcon          from '@material-ui/icons/CheckCircle';
import ErrorIcon                from '@material-ui/icons/Error';
import InfoIcon                 from '@material-ui/icons/Info';
import CloseIcon                from '@material-ui/icons/Close';
import green                    from '@material-ui/core/colors/green';
import amber                    from '@material-ui/core/colors/amber';
import IconButton               from '@material-ui/core/IconButton';
import Snackbar                 from '@material-ui/core/Snackbar';
import SnackbarContent          from '@material-ui/core/SnackbarContent';
import WarningIcon              from '@material-ui/icons/Warning';
import { withStyles }           from '@material-ui/core/styles';
import { connect }              from "react-redux";
import { closeNotify }          from "../../actions/notifyActions";
import { NOTIFY_HIDE_DURATION } from "../../constants";

const variantIcon = {
    success: CheckCircleIcon,
    warning: WarningIcon,
    error  : ErrorIcon,
    info   : InfoIcon,
};

class NotifyMessage extends Component {

    handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        this.props.closeNotify();
    };

    render() {
        const { classes, className, onClose, variant } = this.props;
        const { open, message } = this.props.notify;
        const Icon = variantIcon[ variant ];

        return (
            <Snackbar
                anchorOrigin={{
                    vertical  : 'bottom',
                    horizontal: 'left',
                }}
                open={open}
                autoHideDuration={NOTIFY_HIDE_DURATION}
                onClose={this.handleClose}
            >
                <SnackbarContent
                    className={classNames(classes[ variant ], className)}
                    aria-describedby="client-snackbar"
                    message={
                        <span id="client-snackbar" className={classes.message}>
                            <Icon className={classNames(classes.icon, classes.iconVariant)}/>
                            {message}
                        </span>
                    }
                    action={[
                        <IconButton
                            key="close"
                            aria-label="Close"
                            color="inherit"
                            className={classes.close}
                            onClick={onClose}
                        >
                            <CloseIcon className={classes.icon}/>
                        </IconButton>,
                    ]}
                />
            </Snackbar>
        );
    }
}

NotifyMessage.propTypes = {
    classes    : PropTypes.object.isRequired,
    notify     : PropTypes.object.isRequired,
    className  : PropTypes.string,
    onClose    : PropTypes.func,
    variant    : PropTypes.oneOf([ 'success', 'warning', 'error', 'info' ]).isRequired,
    closeNotify: PropTypes.func.isRequired,
    open       : PropTypes.bool.isRequired
};


const styles = theme => ({
    success    : {
        backgroundColor: green[ 600 ],
    },
    error      : {
        backgroundColor: theme.palette.error.dark,
    },
    info       : {
        backgroundColor: theme.palette.primary.dark,
    },
    warning    : {
        backgroundColor: amber[ 700 ],
    },
    icon       : {
        fontSize: 20,
    },
    iconVariant: {
        opacity    : 0.9,
        marginRight: theme.spacing.unit,
    },
    message    : {
        display   : 'flex',
        alignItems: 'center',
    },
});

const mapStateToProps = state => ({
    notify: state.notify
});

export default connect(mapStateToProps, { closeNotify })(withStyles(styles)(NotifyMessage));
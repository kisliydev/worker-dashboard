import React, { Component } from 'react';
import PropTypes            from 'prop-types';
import { Redirect }         from 'react-router-dom';
import { connect }          from 'react-redux';
import Avatar               from '@material-ui/core/Avatar';
import Button               from '@material-ui/core/Button';
import FormControl          from '@material-ui/core/FormControl';
import Input                from '@material-ui/core/Input';
import InputLabel           from '@material-ui/core/InputLabel';
import LockOutlinedIcon     from '@material-ui/icons/LockOutlined';
import Paper                from '@material-ui/core/Paper';
import Typography           from '@material-ui/core/Typography';
import FormHelperText       from "@material-ui/core/FormHelperText";
import withStyles           from '@material-ui/core/styles/withStyles';

import { loginUser } from '../../actions/authActions';

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            redirectToReferrer: false,
            email             : '',
            password          : '',
            errors            : {}
        };
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.errors !== prevState.errors) {
            return { errors: prevState.errors };
        }

        return null;
    }

    componentDidMount() {
        this.isAuthenticated();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.errors !== this.props.errors) {
            this.setState({
                errors: this.props.errors
            });
        }
        this.isAuthenticated();
    }

    isAuthenticated() {
        if (this.props.auth.isAuthenticated) {
            this.props.history.push('/');
        }
    }

    onSubmit = (e) => {
        e.preventDefault();

        const userData = {
            email   : this.state.email,
            password: this.state.password
        };

        this.props.loginUser(userData);
    };

    onChange = (e) => {
        this.setState({
            errors           : {},
            [ e.target.name ]: e.target.value
        });
    };

    render() {

        const { from } = this.props.location.state || { from: { pathname: "/" } };
        const { redirectToReferrer, errors } = this.state;
        const { classes } = this.props;

        //TODO Check this !
        if (redirectToReferrer) return <Redirect to={from}/>;

        return (
            <main className={classes.main}>
                <Paper className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <LockOutlinedIcon/>
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Login
                    </Typography>
                    <form className={classes.form} noValidate onSubmit={this.onSubmit}>
                        <FormControl margin="normal" required fullWidth error={!!errors.email}>
                            <InputLabel htmlFor="email">Email Address</InputLabel>
                            <Input
                                id="email"
                                name="email"
                                autoComplete="email"
                                autoFocus
                                value={this.state.email}
                                onChange={this.onChange}
                            />
                            {errors.email && <FormHelperText>{errors.email}</FormHelperText>}
                        </FormControl>
                        <FormControl margin="normal" required fullWidth error={!!errors.password}>
                            <InputLabel htmlFor="password">Password</InputLabel>
                            <Input
                                id="password"
                                name="password"
                                type="password"
                                autoComplete="current-password"
                                value={this.state.password}
                                onChange={this.onChange}
                            />
                            {errors.password && <FormHelperText>{errors.password}</FormHelperText>}
                        </FormControl>
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                        >
                            Login
                        </Button>
                    </form>
                </Paper>
            </main>
        );
    }
}

const styles = theme => ({
    main  : {
        width                                                     : 'auto',
        display                                                   : 'block', // Fix IE 11 issue.
        marginLeft                                                : theme.spacing.unit * 3,
        marginRight                                               : theme.spacing.unit * 3,
        [ theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2) ]: {
            width      : 400,
            marginLeft : 'auto',
            marginRight: 'auto',
        },
    },
    paper : {
        marginTop    : theme.spacing.unit * 8,
        display      : 'flex',
        flexDirection: 'column',
        alignItems   : 'center',
        padding      : `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
    avatar: {
        margin         : theme.spacing.unit,
        backgroundColor: theme.palette.secondary.main,
    },
    form  : {
        width    : '100%', // Fix IE 11 issue.
        marginTop: theme.spacing.unit,
    },
    submit: {
        marginTop: theme.spacing.unit * 3,
    },
});

Login.propTypes = {
    classes  : PropTypes.object.isRequired,
    loginUser: PropTypes.func.isRequired,
    auth     : PropTypes.object.isRequired,
    errors   : PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth  : state.auth,
    errors: state.errors
});

export default connect(mapStateToProps, { loginUser })(withStyles(styles)(Login));
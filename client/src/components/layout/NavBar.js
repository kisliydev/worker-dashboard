import React, { Component, Fragment } from 'react';
import { Link }                       from 'react-router-dom';
import PropTypes                      from 'prop-types';
import { connect }                    from 'react-redux';
import { logoutUser }                 from '../../actions/authActions';

import AppBar         from '@material-ui/core/AppBar';
import Toolbar        from '@material-ui/core/Toolbar';
import Typography     from '@material-ui/core/Typography';
import Button         from '@material-ui/core/Button';
import { withStyles } from "@material-ui/core/styles/index";

class NavBar extends Component {

    logoutClick = (e) => {
        e.preventDefault();
        this.props.logoutUser();
    };

    render() {
        const { classes, auth, location } = this.props;

        const LoginLink = (props) => <Link to='/login' {...props}/>;
        const RegistrationLink = (props) => <Link to='/registration' {...props}/>;

        const guestLinks = (
            <Fragment>
                <Button
                    color="primary"
                    variant={location.pathname === '/registration' ? 'outlined' : 'text'}
                    component={RegistrationLink}
                >
                    Registration
                </Button>

                <Button
                    color="primary"
                    variant={location.pathname === '/login' ? 'outlined' : 'text'}
                    component={LoginLink}
                >
                    Login
                </Button>
            </Fragment>
        );

        const authLinks = (
            <Button color="primary" variant="outlined" onClick={this.logoutClick}>
                Logout
            </Button>
        );

        return (
            <AppBar position="fixed" color="default">
                <Toolbar>
                    <Typography variant="h6" color="primary" noWrap className={classes.toolbarTitle}>
                        Dashboard
                    </Typography>
                    {auth.isAuthenticated ? authLinks : guestLinks}
                </Toolbar>
            </AppBar>
        );
    }
}

const styles = () => ({
    toolbarTitle: {
        flex: 1
    }
});

NavBar.propTypes = {
    logoutUser: PropTypes.func.isRequired,
    auth      : PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(mapStateToProps, { logoutUser })(withStyles(styles)(NavBar));

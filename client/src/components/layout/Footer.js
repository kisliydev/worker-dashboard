import React, { Component, Fragment } from 'react';
import PropTypes                      from 'prop-types';
import { connect }                    from 'react-redux';
import Typography                     from '@material-ui/core/Typography';
import { withStyles }                 from "@material-ui/core/styles/index";
import NotifyMessage                  from "../common/NotifyMessage";

class Footer extends Component {

    render() {
        const { classes } = this.props;
        const { open, type, message } = this.props.notify;

        return (
            <Fragment>
                <footer className={classes.footer}>
                    <Typography variant="subtitle1" align="center" color="textSecondary">
                        &copy; {new Date().getFullYear()} Maxim Kisliy
                    </Typography>
                </footer>
                <NotifyMessage open={open} variant={type} message={message}/>
            </Fragment>
        );
    }
}

const styles = theme => ({
    footer: {
        padding: theme.spacing.unit,
    }
});

Footer.propTypes = {
    notify: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    notify: state.notify
});

export default connect(mapStateToProps)(withStyles(styles)(Footer));

import React, { Component }             from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Provider }                     from 'react-redux';
import PropTypes                        from 'prop-types';
import jwt_decode                       from 'jwt-decode';

import { withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import CssBaseline                      from '@material-ui/core/CssBaseline';

import Login        from './components/auth/Login';
import Registration from './components/auth/Registration';
import Workers      from './components/workers/Workers';
import PrivateRoute from './components/common/PrivateRoute';
import NotFound     from "./components/common/NotFound";
import NavBar       from "./components/layout/NavBar";
import Footer       from "./components/layout/Footer";

import setAuthToken                   from './utils/setAuthToken';
import { setCurrentUser, logoutUser } from './actions/authActions';

import store from "./store";
import theme from './theme'

// // Check for token
if (localStorage.jwtToken) {
    // Set auth token header auth
    setAuthToken(localStorage.jwtToken);
    // Decode token and get user info and exp
    const decoded = jwt_decode(localStorage.jwtToken);
    // Set user and isAuthenticated
    store.dispatch(setCurrentUser(decoded));

    // Check for expired token
    const currentTime = Date.now() / 1000;
    if (decoded.exp < currentTime) {
        // Logout user
        store.dispatch(logoutUser());
        // Redirect to login
        window.location.href = '/login';
    }
}

class App extends Component {
    render() {
        const { classes } = this.props;

        return (
            <Provider store={store}>
                <BrowserRouter>
                    <MuiThemeProvider theme={theme}>
                        <div className={classes.app}>
                            <CssBaseline/>
                            <Switch>
                                <NavBar/>
                            </Switch>
                            <main className={classes.content}>
                                <Switch>
                                    <Route exact path="/login" component={Login}/>
                                    <Route exact path="/registration" component={Registration}/>
                                    <PrivateRoute exact path="/" component={Workers}/>
                                    <Route path="/" component={NotFound}/>
                                </Switch>
                            </main>
                            <Footer/>
                        </div>
                    </MuiThemeProvider>
                </BrowserRouter>
            </Provider>
        );
    }
}


const styles = theme => ({
    app    : {
        display      : 'flex',
        minHeight    : '100vh',
        flexDirection: 'column'
    },
    content: {
        flexGrow: 1,
        padding : theme.spacing.unit * 3,
        overflow: 'auto'
    }
});

App.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(App);

const express = require('express');
const passport = require('passport');
const router = express.Router();

// Worker model
const Worker = require('../../../models/Worker');

// Validation
const validateWorkerInput = require('../../../validation/worker');

/**
 * @route  GET api/{API_VERSION}/workers/test
 * @desc   Test workers route
 * @access Public
 */
router.get('/test', (req, res) => res.json({ msg: "Workers endpoint works" }));

/**
 * @route  GET api/{API_VERSION}/workers
 * @desc   Get workers
 * @access Private
 */
router.get('/', passport.authenticate('jwt', { session: false }), (req, res) => {
    Worker.find({ $and: [ { user: req.user.id } ] },)
          .sort({ date: "desc" })
          .then(workers => {
              if (workers.length === 0) {
                  return res.status(404).json({ noworkerfound: "No workers found" });
              }
              res.json(workers);
          })
          .catch(() => res.status(404).json({ noworkerfound: 'No workers found' }));
});

/**
 * @route  GET api/{API_VERSION}/workers/:id
 * @desc   Get worker by id
 * @access Private
 */
router.get('/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
    Worker.findById(req.params.id)
          .then(worker => res.json(worker))
          .catch(() => res.status(404).json({ noworkerfound: 'No worker found with that ID' }));
});

/**
 * @route  POST api/{API_VERSION}/workers
 * @desc   Create worker
 * @access Private
 */
router.post('/', passport.authenticate('jwt', { session: false }), (req, res) => {
    const { errors, isValid } = validateWorkerInput(req.body);

    // Check validation
    if (!isValid) {
        return res.status(400).json(errors);
    }

    // Create demo records
    // for(let i = 0; i < 3000; i++) {
    //
    //     const newWorker = new Worker({
    //         first_name: req.body.first_name+' '+i,
    //         last_name : req.body.last_name+' '+i,
    //         gender    : req.body.gender,
    //         phone     : req.body.phone,
    //         salary    : req.body.salary,
    //         position  : req.body.position+' '+i,
    //         user      : req.user.id
    //     });
    //
    //     newWorker.save().then(worker => res.json(worker));
    // }

    const newWorker = new Worker({
        first_name: req.body.first_name,
        last_name : req.body.last_name,
        gender    : req.body.gender,
        phone     : req.body.phone,
        salary    : req.body.salary,
        position  : req.body.position,
        user      : req.user.id
    });

    newWorker.save().then(worker => res.json(worker));
});

/**
 * @route  Put api/{API_VERSION}/workers/:id
 * @desc   Update worker
 * @access Private
 */
router.put('/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
    const { errors, isValid } = validateWorkerInput(req.body);

    // Check validation
    if (!isValid) {
        return res.status(400).json(errors);
    }

    const worker = {
        first_name: req.body.first_name,
        last_name : req.body.last_name,
        gender    : req.body.gender,
        phone     : req.body.phone,
        salary    : req.body.salary,
        position  : req.body.position,
        user      : req.user.id
    };

    Worker.findOneAndUpdate(
        { $and: [ { _id: req.params.id }, { user: req.user.id } ] },
        worker,
        (err) => {
            if (err) {
                return res.status(404).json({ noworkerfound: 'No worker found' });
            }
            res.json({ success: true });
        });
});

/**
 * @route  DELETE api/{API_VERSION}/workers/:id
 * @desc   Delete worker
 * @access Private
 */
router.delete('/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
    Worker.findOneAndDelete(
        { $and: [ { _id: req.params.id }, { user: req.user.id } ] },
        (err) => {
            if (err) {
                return res.status(404).json({ noworkerfound: 'No worker found' });
            }
            res.json({ success: true });
        });
});


/**
 * @route  POST api/{API_VERSION}/workers/delete
 * @desc   Delete multiple workers
 * @access Private
 */
router.post('/delete', passport.authenticate('jwt', { session: false }), (req, res) => {
    Worker.deleteMany(
        { $and: [ { _id: { $in: req.body } }, { user: req.user.id } ] },
        (err) => {
            if (err) {
                return res.status(404).json({ noworkerfound: 'No workers found' });
            }
            res.json({ success: true });
        });
});


module.exports = router;